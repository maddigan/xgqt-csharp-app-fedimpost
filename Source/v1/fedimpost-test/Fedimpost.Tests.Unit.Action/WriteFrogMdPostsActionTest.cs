/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Microsoft.Extensions.Logging;
using NUnit.Framework;

using Fedimpost.Action.Configuration;
using Fedimpost.Action.PostsAction;
using Fedimpost.Net.DTO;

namespace Fedimpost.Tests.Unit.Action;

[TestFixture]
public class WriteFrogMdPostsActionTest
{

    private ReadOnlyCollection<FediPostDTO> _posts;

    private PostsActionConfigurationDTO _actionConfiguration;

    [SetUp]
    public void SetUp()
    {
        List<FediPostDTO> postList = new()
        {
            new FediPostDTO()
            {
                Id = "1",
                CreatedAt = DateTime.Now,
                Url = new Uri("https://example.com"),
                Content = "Lorem ipsum",
            }
        };

        _posts = postList.AsReadOnly();


        #region logger setup

        using var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });

        var logger = loggerFactory.CreateLogger<WriteFrogMdPostsActionTest>();

        #endregion


        _actionConfiguration = new()
        {
            Logger = logger,
            Limit = 10,
            OutputDirectory = "/tmp",
            WriteDryRun = true,
            Interactive = false,
        };
    }

    [Test]
    public void WriteFrogPostsAction_Passes()
    {
        Assert.That(
            () =>
            {
                WriteFrogMdPostsAction writeFrogMdPostsAction = new()
                {
                    Posts = _posts,
                    ActionConfiguration = _actionConfiguration,
                };

                writeFrogMdPostsAction.Run();
            },
            Throws.Nothing
        );
    }

}
