#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import path

from subprocess import run


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    source_root = path.realpath(path.join(script_root, ".."))
    admin_path = path.join(source_root, "admin")

    chdir(source_root)

    make_script_path = path.join(admin_path, "make_fedimpost_test.py")

    run(
        [
            "python3",
            make_script_path,
            "clean",
        ],
        check=True,
    )


if __name__ == "__main__":
    main()
