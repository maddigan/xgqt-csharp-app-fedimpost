#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import environ
from os import path

from dataclasses import dataclass
from shutil import which
from subprocess import run
from sys import argv


@dataclass
class Make():
    """! Configure make with passed in settings."""

    subproject_path: str
    dotnet_executable: str

    def run(self, targets):
        """! Run make."""

        make_executable = which("gmake") or which("make")

        run(
            [
                make_executable,
                "-s",
                "-C",
                self.subproject_path,
                f"DOTNET={self.dotnet_executable}",
            ] + targets,
            check=True,
        )


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    source_root = path.realpath(path.join(script_root, ".."))
    build_path = path.realpath(path.join(source_root, "..", "..", "Build"))

    build_dir_path = path.join(build_path, "v1_dotnet_BuildDir")

    subproject_path = path.join(source_root, "fedimpost-test")
    dotnet_output_directory = path.join(build_dir_path, "output")

    make_targets = argv[1::]
    dotnet_executable = "dotnet"

    if "DOTNET_ROOT" in environ:
        dotnet_executable = path.join(environ["DOTNET_ROOT"], "dotnet")

    print(f" * Build output: {dotnet_output_directory}")

    print(f" * Entering directory: {subproject_path}")
    chdir(subproject_path)

    make = Make(subproject_path, dotnet_executable)

    make.run(make_targets)


if __name__ == "__main__":
    main()
