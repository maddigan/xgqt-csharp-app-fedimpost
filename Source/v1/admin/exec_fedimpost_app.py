#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import environ
from os import path

from subprocess import run
from sys import argv


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    source_root = path.realpath(path.join(script_root, ".."))
    build_path = path.realpath(path.join(source_root, "..", "..", "Build"))

    build_dir_path = path.join(build_path, "v1_dotnet_BuildDir")
    dotnet_output_directory = path.join(build_dir_path, "output")

    dotnet_dll = path.join(dotnet_output_directory, "Fedimpost.dll")
    dotnet_executable = "dotnet"
    exec_opts = argv[1::]

    if "DOTNET_ROOT" in environ:
        dotnet_executable = path.join(environ["DOTNET_ROOT"], "dotnet")

    if not exec_opts:
        exec_opts = ["--help"]

    run(
        [
            dotnet_executable,
            "exec",
            dotnet_dll,
        ]
        + exec_opts,
        check=True,
    )


if __name__ == "__main__":
    main()
