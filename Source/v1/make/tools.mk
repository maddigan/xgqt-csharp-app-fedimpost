# -*- make -*-

MAKE            ?= make

CP              := cp
DOTNET          := dotnet
FIND            := find
GZEXE           := gzexe
PYTHON          := python3
RM              := rm -r
