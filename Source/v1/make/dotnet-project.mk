# -*- make -*-

DOTNET-OUTPUT           :=

DOTNET-CONFIGURATION    := Release
DOTNET-EXEC-OPTS        :=

DOTNET-SOLUTION         :=
DOTNET-PROJECT          :=
DOTNET-EXEC-DLL         :=

.PHONY: all
all:

.PHONY: find-rm-bin
find-rm-bin:
	$(FIND) . -type d -name "bin" -exec $(RM) {} +

.PHONY: find-rm-obj
find-rm-obj:
	$(FIND) . -type d -name "bin" -exec $(RM) {} +

.PHONY: clean
clean:
	$(MAKE) find-rm-bin
	$(MAKE) find-rm-obj

	-$(DOTNET) clean							\
		--configuration $(DOTNET-CONFIGURATION)	\
		--verbosity quiet						\
		-maxCpuCount:1							\
		$(DOTNET-SOLUTION)

.PHONY: restore
restore:
	$(DOTNET) restore							\
		--verbosity quiet						\
		-maxCpuCount:1							\
		$(DOTNET-SOLUTION)

.PHONY: build
build: restore
build:
	$(DOTNET) build								\
		--configuration $(DOTNET-CONFIGURATION)	\
		--no-restore							\
		--no-self-contained						\
		--output $(DOTNET-OUTPUT)				\
		--verbosity quiet						\
		-maxCpuCount:1							\
		$(DOTNET-PROJECT)

.PHONY: analyze
analyze:
	$(MAKE) DOTNET-CONFIGURATION="Debug" clean
	$(MAKE) DOTNET-CONFIGURATION="Debug" restore

	$(DOTNET) build								\
		--configuration Debug	                \
		--no-self-contained						\
		--verbosity minimal						\
		-maxCpuCount:1							\
		$(DOTNET-SOLUTION)

.PHONY: format
format: restore
format:
	$(DOTNET) format							\
		--no-restore							\
		--verbosity detailed					\
		$(DOTNET-SOLUTION)

.PHONY: test
test: restore
test:
	$(DOTNET) test								\
		--configuration $(DOTNET-CONFIGURATION)	\
		--no-restore							\
		--verbosity quiet						\
		-maxCpuCount:1							\
		$(DOTNET-SOLUTION)

.PHONY: run
run: build
run:
	$(DOTNET) exec $(DOTNET-EXEC-DLL)		   \
		$(DOTNET-EXEC-OPTS)
