/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Microsoft.Extensions.Logging;

using Fedimpost.Action;

namespace Fedimpost;

public static class Program
{

    public static int Main(string[] argv)
    {

        #region logger setup

        using var loggerFactory = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });

        var logger = loggerFactory.CreateLogger<CommandLineAction>();

        #endregion


        CommandLineAction commandLineAction = new()
        {
            CliArguments = argv,
            Logger = logger,
        };

        return commandLineAction.Run();
    }

}
