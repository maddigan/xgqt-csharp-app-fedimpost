/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Fedimpost.Action.Base;
using Fedimpost.Action.Enums;
using Fedimpost.Action.PostsAction;
using Fedimpost.Converter.HtmlToMd.Exceptions;
using Fedimpost.Net.DAO;
using Fedimpost.Net.DTO;

namespace Fedimpost.Action.Manager;

public class FedimpostActionManager : PostsActionConfigurationUserBase
{

    public FediAccountDTO FediAccount { get; init; }

    private async Task<ReadOnlyCollection<FediPostDTO>> GetPosts()
    {
        FediPostDAO fediPostDAO = new(FediAccount);

        var posts = await fediPostDAO
            .GetPostsAsync(ActionConfiguration.Limit)
            .ConfigureAwait(false);

        return posts;
    }

    public async Task Dispatch(OutputType outputTypeEnum)
    {
        PostsActionBase postsAction;

        switch (outputTypeEnum)
        {
            case OutputType.Console:
                postsAction = new ShowPostsAction();

                break;

            case OutputType.FrogMd:
                postsAction = new WriteFrogMdPostsAction();

                break;

            case OutputType.SimpleMd:
                postsAction = new WriteSimpleMdPostsAction();

                break;

            default:
                ActionConfiguration.Logger.LogError(
                    $"ERROR: Unknown output type, given {outputTypeEnum}" + "\n"
                    + "Valid values: Console, FrogMd, SimpleMd."
                );

                throw new Exception("unknown output type");
        }

        postsAction.ActionConfiguration = ActionConfiguration;

        postsAction.Posts = await GetPosts()
            .ConfigureAwait(false);

        try
        {
            postsAction.Run();
        }
        catch (HtmlToMdConversionException exception)
        {
            ActionConfiguration.Logger.LogError(
                exception,
                "content of this post could not be converted, skipping"
            );
        }
        catch (Exception)
        {
            throw;
        }
    }

}
