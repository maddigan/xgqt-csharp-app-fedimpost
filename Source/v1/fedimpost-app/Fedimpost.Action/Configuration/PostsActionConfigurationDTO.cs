/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Microsoft.Extensions.Logging;

namespace Fedimpost.Action.Configuration;

public class PostsActionConfigurationDTO
{

    public ILogger Logger { get; init; }

    public int Limit { get; init; }

    public string OutputDirectory { get; init; }

    public bool WriteDryRun { get; init; }

    public bool Interactive { get; init; }

}
