/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.CommandLine;

using Fedimpost.Action.Enums;

namespace Fedimpost.Action.Command;

/// <summary>
/// Customized version of RootCommand for Fedimpost purposes.
/// </summary>
public class CommandLineRootCommand : RootCommand
{

    #region create required options

    public static readonly Option<string> instanceOption = new(
        new[] { "-i", "--instance" },
        "Fediverse instance to use"
    )
    {
        Arity = ArgumentArity.ExactlyOne,
        IsRequired = true,
    };
    public static readonly Option<string> userOption = new(
        new[] { "-u", "--user" },
        "user account to lookup"
    )
    {
        Arity = ArgumentArity.ExactlyOne,
        IsRequired = true,
    };

    #endregion


    public static readonly Option<int> limitOption = new(
        new[] { "-l", "--limit" },
        "limit of parsed posts"
    )
    {
        Arity = ArgumentArity.ExactlyOne,
    };

    public static readonly Option<string> outputType = new(
        new[] { "-t", "--type" },
        "type of generated output"
    )
    {
        Arity = ArgumentArity.ExactlyOne,
    };

    public static readonly Option<string> outputDirectory = new(
        new[] { "-o", "--output" },
        "output directory of generated posts"
    )
    {
        Arity = ArgumentArity.ExactlyOne,
    };

    public static readonly Option<bool> dryRun = new(
        new[] { "-d", "--dry-run" },
        "do not create files, only print what would happen"
    );

    public static readonly Option<bool> interactive = new(
        new[] { "-I", "--interactive" },
        "interactive mode"
    );

    public CommandLineRootCommand()
    {
        Name = "fedimpost";
        Description = "Fediverse post import to blog frameworks";


        #region set default option values

        limitOption.SetDefaultValue(20);
        outputType.SetDefaultValue(OutputType.Console.ToString());
        outputDirectory.SetDefaultValue(".");
        dryRun.SetDefaultValue(false);
        interactive.SetDefaultValue(false);

        #endregion


        #region add options to the root command

        Add(instanceOption);
        Add(userOption);
        Add(limitOption);
        Add(outputType);
        Add(outputDirectory);
        Add(dryRun);
        Add(interactive);

        #endregion
    }

}
