/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Fedimpost.Action.Enums;

/// <summary>
/// Emum representing a output generated form posts.
/// </summary>
public enum OutputType
{

    // Default: unknown, none of the bwlow.
    Unknown,

    // Console output.
    Console,

    // Frog MarkDown blog post.
    FrogMd,

    // Simple MarkDown, similar to "FrogMd".
    SimpleMd,

}
