/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.ObjectModel;

using Fedimpost.Action.Interfaces;
using Fedimpost.Converter.HtmlToMd.DAO;
using Fedimpost.Net.DTO;

namespace Fedimpost.Action.Base;

public class PostsActionBase : PostsActionConfigurationUserBase, IPostsAction
{

    public ReadOnlyCollection<FediPostDTO> Posts { get; set; }

    internal readonly HtmlToMdDAO _htmlToMd;

    protected PostsActionBase()
    {
        _htmlToMd = new();
    }

    public virtual void Run()
    {

    }

}
