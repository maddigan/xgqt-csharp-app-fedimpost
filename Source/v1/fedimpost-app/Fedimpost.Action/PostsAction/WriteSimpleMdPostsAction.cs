/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Globalization;
using System.IO;

using Microsoft.Extensions.Logging;

using Fedimpost.Action.Base;
using Fedimpost.Blog.Template;
using Fedimpost.Converter.HtmlToMd.Exceptions;

namespace Fedimpost.Action.PostsAction;

public class WriteSimpleMdPostsAction : PostsActionBase
{

    private readonly CultureInfo _posixCulture;

    public WriteSimpleMdPostsAction() : base()
    {
        _posixCulture = new("en-US-POSIX");
    }

    // TODO: Move to utils or similar?
    private bool AskYesNoQuestion(string question)
    {
        while (true)
        {
            Console.Write($" Q: {question} [Y/n]: ");

            string userInput = Console
                .ReadLine()?.Trim()
                .ToUpperInvariant();

            switch (userInput)
            {
                case null:
                case "":
                case "Y":
                    return true;

                case "N":
                    return false;

                default:
                    ActionConfiguration.Logger.LogWarning(
                        "Invalid input. Please enter 'Y' for Yes or 'N' for No."
                    );

                    break;
            }
        }
    }

    public override void Run()
    {
        foreach (var post in Posts)
        {
            string postFileDate = post.CreatedAt.ToString(
                "yyyy-MM-dd",
                _posixCulture
            );
            string postFileName = $"{postFileDate}-fedi-import-{post.Id}.md";
            string postFileOutputPath = Path.Combine(
                ActionConfiguration.OutputDirectory,
                postFileName
            );

            try
            {
                string convertedContent = _htmlToMd.Convert(post.Content);

                SimplePostTemplate SimplePostTemplate = new()
                {
                    Url = post.Url,
                    Content = convertedContent,
                };

                string blogPostContent = SimplePostTemplate.Create();

                bool doWrite = true;

                if (ActionConfiguration.Interactive)
                {
                    Console.WriteLine(
                        "Those would be the file contents:" + "\n"
                        + blogPostContent
                    );

                    doWrite = AskYesNoQuestion(
                        $"Write file {postFileOutputPath}?"
                    );
                }

                if (!doWrite)
                {
                    ActionConfiguration.Logger.LogInformation(
                        $"Selected to skip file: {postFileOutputPath}"
                    );
                }
                else if (ActionConfiguration.WriteDryRun)
                {
                    ActionConfiguration.Logger.LogInformation(
                        $"Dry run, skipping file: {postFileOutputPath}"
                    );
                }
                else
                {
                    ActionConfiguration.Logger.LogInformation(
                        $"Creating post file: {postFileOutputPath}"
                    );

                    File.WriteAllText(
                        path: postFileOutputPath,
                        contents: blogPostContent
                    );
                }
            }
            catch (HtmlToMdConversionException exception)
            {
                ActionConfiguration.Logger.LogError(
                    exception,
                    "content of this post could not be converted, skipping"
                );
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
