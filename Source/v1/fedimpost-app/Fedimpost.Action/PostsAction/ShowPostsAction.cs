/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

using Microsoft.Extensions.Logging;

using Fedimpost.Action.Base;
using Fedimpost.Converter.HtmlToMd.Exceptions;

namespace Fedimpost.Action.PostsAction;

public class ShowPostsAction : PostsActionBase
{

    public override void Run()
    {
        foreach (var post in Posts)
        {
            ActionConfiguration.Logger.LogInformation(
                $"Post from: {post.CreatedAt}" + "\n"
                + $"Post URL: {post.Url}"
            );

            try
            {
                string convertedContent = _htmlToMd.Convert(post.Content);

                ActionConfiguration.Logger.LogInformation(convertedContent);
            }
            catch (HtmlToMdConversionException exception)
            {
                ActionConfiguration.Logger.LogError(
                    exception,
                    "content of this post could not be converted, skipping"
                );
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
