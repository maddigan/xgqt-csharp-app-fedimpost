/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.CommandLine;
using System.CommandLine.Parsing;

using Microsoft.Extensions.Logging;

using Fedimpost.Action.Command;
using Fedimpost.Action.Configuration;
using Fedimpost.Action.Enums;
using Fedimpost.Action.Manager;
using Fedimpost.Net.DTO;

namespace Fedimpost.Action;

public class CommandLineAction
{

    public string[] CliArguments { get; init; }

    public ILogger Logger { get; init; }

    private readonly CommandLineRootCommand _rootCommand;

    public CommandLineAction()
    {
        _rootCommand = new();

        _rootCommand.SetHandler(async (context) =>
        {
            ParseResult options = context.ParseResult;

            FediAccountDTO fediAccount = new()
            {
                FediInstance = options
                    .GetValueForOption(CommandLineRootCommand.instanceOption),
                UserName = options
                    .GetValueForOption(CommandLineRootCommand.userOption)
            };

            PostsActionConfigurationDTO actionConfiguration = new()
            {
                Logger = Logger,

                Limit = options
                    .GetValueForOption(CommandLineRootCommand.limitOption),
                OutputDirectory = options
                    .GetValueForOption(CommandLineRootCommand.outputDirectory),
                WriteDryRun = options
                    .GetValueForOption(CommandLineRootCommand.dryRun),
                Interactive = options
                    .GetValueForOption(CommandLineRootCommand.interactive),
            };

            FedimpostActionManager fedimpostActionManager = new()
            {
                FediAccount = fediAccount,
                ActionConfiguration = actionConfiguration,
            };

            string outputTypeString = options
                .GetValueForOption(CommandLineRootCommand.outputType);

            bool enumParseSuccess = Enum.TryParse<OutputType>(
                value: outputTypeString,
                result: out OutputType outputTypeEnum
            );

            if (!enumParseSuccess)
            {
                Logger.LogWarning($"failed to parse {outputTypeString}");
            }

            Logger.LogInformation(
                $"account: {fediAccount.UserName}@{fediAccount.FediInstance}"
            );

            await fedimpostActionManager
                .Dispatch(outputTypeEnum)
                .ConfigureAwait(false);
        });
    }

    public int Run()
    {
        return _rootCommand.Invoke(CliArguments);
    }

}
