/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

using Fedimpost.Converter.HtmlToMd.Exceptions;

namespace Fedimpost.Converter.HtmlToMd.DAO;

public class HtmlToMdDAO
{

    private readonly ReverseMarkdown.Config _config;

    private readonly ReverseMarkdown.Converter _converter;

    public HtmlToMdDAO()
    {
        _config = new ReverseMarkdown.Config
        {
            UnknownTags = ReverseMarkdown.Config.UnknownTagsOption.Raise,
            GithubFlavored = false,
            DefaultCodeBlockLanguage = "shell",
            RemoveComments = false,
            SmartHrefHandling = true,
            ListBulletChar = '*',
            TableWithoutHeaderRowHandling = ReverseMarkdown.Config.TableWithoutHeaderRowHandlingOption.EmptyRow,
        };

        _converter = new ReverseMarkdown.Converter(_config);
    }

    public string Convert(string input)
    {
        try
        {
            return _converter.Convert(input);
        }
        catch (Exception exception)
        {
            throw new HtmlToMdConversionException(
                message: "could not convert input, given \"{input}\"",
                inner: exception
            );
        }
    }

}
