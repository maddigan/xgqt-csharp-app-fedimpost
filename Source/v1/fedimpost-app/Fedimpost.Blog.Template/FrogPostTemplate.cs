/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

using Fedimpost.Blog.Template.Base;
using Fedimpost.Blog.Template.Common;

namespace Fedimpost.Blog.Template;

public class FrogPostTemplate : PostTemplateBase
{

    public DateTime CreatedAt { get; init; }

    public string Create()
    {
        return @$"{ConstantCommon.Tab}Title: {TitleFromContent}...
{ConstantCommon.Tab}Date: {CreatedAt:s}
{ConstantCommon.Tab}Tags: {string.Join(", ", DefaultTags)}

{TrimmedContent}

{HtmlEpilogSmall}";
    }

}
