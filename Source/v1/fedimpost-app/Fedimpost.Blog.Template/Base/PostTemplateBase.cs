/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Fedimpost.Blog.Template.Common;
using Fedimpost.Blog.Template.Util;

namespace Fedimpost.Blog.Template.Base;

public class PostTemplateBase
{

    /// <summary>
    /// Post URL.
    /// </summary>
    public Uri Url { get; init; }

    /// <summary>
    /// Post content.
    /// </summary>
    public string Content { get; init; }

    /// <summary>
    /// Returns article title based on content.
    /// </summary>
    public string TitleFromContent
    {
        get
        {
            return TitleUtil.ContentToTitle(Content);
        }
    }

    /// <summary>
    /// Returns trimmed content.
    /// </summary>
    public string TrimmedContent
    {
        get
        {
            return ContentUtil.TrimContent(Content);
        }
    }

    public ReadOnlyCollection<string> DefaultTags
    {
        get
        {
            List<string> tags = new() { };

            tags.AddRange(ConstantCommon.DefaultTags);
            tags.Add(Url.Host);

            return tags.AsReadOnly();
        }
    }

    public string ShortWebLink
    {
        get
        {
            return UrlUtil.ShortenUrl(Url);
        }
    }

    /// <summary>
    /// Retuns a small HTML epilog.
    /// </summary>
    public string HtmlEpilogSmall
    {
        get
        {
            return @$"<small>
{ConstantCommon.Tab}Imported via [Fedimpost](https://gitlab.com/xgqt/xgqt-csharp-app-fedimpost)
{ConstantCommon.Tab}from [{ShortWebLink}]({Url})
</small>";
        }
    }

}
