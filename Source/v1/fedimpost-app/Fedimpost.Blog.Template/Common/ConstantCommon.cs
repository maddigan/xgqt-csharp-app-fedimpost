/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Fedimpost.Blog.Template.Common;

public static class ConstantCommon
{

    public const string Tab = "    ";

    public static readonly ReadOnlyCollection<string> DefaultTags =
        new List<string> { "fediverse" }.AsReadOnly();

}
