/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Text.RegularExpressions;

namespace Fedimpost.Blog.Template.Util;

public static class ContentUtil
{

    private const string _trailingSpaceRegex = @"[ \t]+$";

    public static string TrimContent(string content)
    {
        string contentTrimmed = Regex.Replace(
            input: content,
            pattern: _trailingSpaceRegex,
            replacement: string.Empty,
            options: RegexOptions.Multiline
        );

        return contentTrimmed;
    }

}
