/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace Fedimpost.Blog.Template.Util;

public static class UrlUtil
{

    private const string _httpRegex = @"https?:\/\/";

    public static string ShortenUrl([NotNull] Uri url)
    {
        string urlShort = Regex.Replace(
            input: url.ToString(),
            pattern: _httpRegex,
            replacement: string.Empty
        );

        return urlShort;
    }

}
