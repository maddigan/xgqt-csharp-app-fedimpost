/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Text.RegularExpressions;

namespace Fedimpost.Blog.Template.Util;

public static class TitleUtil
{

    private const string _whitespacesRegex = @"\s+";

    // Poor man's link removal.
    private const string _mdLinkRegex = @"\[([^\[\]]+)\]\(([^()]+)\)";

    // private const string _trailingSpaceRegex = @"[ \t]+$";

    // private const string _httpRegex = @"https?:\/\/";

    public static string ContentToTitle(string content)
    {
        string contentNoLinks = Regex.Replace(
            input: content,
            pattern: _mdLinkRegex,
            replacement: string.Empty,
            options: RegexOptions.Multiline
        );
        string contentSingleWhitespace = Regex.Replace(
            input: contentNoLinks,
            pattern: _whitespacesRegex,
            replacement: " ",
            options: RegexOptions.Multiline
        );
        string contentForTitle = contentSingleWhitespace.Trim();
        string title =
            contentForTitle.Length >= 60
            ? contentForTitle[..60]
            : contentForTitle;

        return title;
    }

}
