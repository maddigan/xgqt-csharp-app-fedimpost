/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Fedimpost.Net.DTO;

/// <summary>
/// Transfer object representing a Fediverse user account.
/// </summary>
public record FediAccountDTO
{

    /// <summary>
    /// Fediverse instance domain.
    /// </summary>
    public string FediInstance { get; init; }

    /// <summary>
    /// Fediverse account user name.
    /// </summary>
    public string UserName { get; init; }

}
