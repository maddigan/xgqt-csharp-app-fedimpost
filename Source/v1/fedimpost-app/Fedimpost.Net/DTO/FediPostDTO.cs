/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Fedimpost.Net.DTO;

/// <summary>
/// Transfer object representing a Fediverse post.
/// </summary>
public record FediPostDTO
{

    public string Id { get; init; }

    public DateTime CreatedAt { get; init; }

    public Uri Url { get; init; }

    public string Content { get; init; }

}
