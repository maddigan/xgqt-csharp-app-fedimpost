/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using Fedimpost.Net.DTO;

namespace Fedimpost.Net.DAO;

public class FediPostDAO
{

    private readonly FediAccountDTO _fediAccount;

    public FediPostDAO(FediAccountDTO fediAccountDTO)
    {
        _fediAccount = fediAccountDTO;
    }

    private static string JsonToJustId(JObject jsonObject)
    {
        jsonObject.TryGetValue("id", out JToken idJson);

        return idJson.Value<string>();
    }

    private async Task<string> GetUserIdAsync()
    {
        Uri url = new($"https://{_fediAccount.FediInstance}/api/v1/accounts/lookup?acct={_fediAccount.UserName}");

        using HttpClient httpClient = new();

        try
        {
            HttpResponseMessage response = await httpClient
                .GetAsync(url)
                .ConfigureAwait(false);

            response.EnsureSuccessStatusCode();

            string jsonContent = await response
                .Content
                .ReadAsStringAsync()
                .ConfigureAwait(false);

            JObject jsonObject = JObject.Parse(jsonContent);

            return JsonToJustId(jsonObject);
        }
        catch (Exception exception)
        {
            throw new Exception("request did not complete successfully", exception);
        }
    }

    private static ReadOnlyCollection<FediPostDTO> JsonArrayToPostsList(JArray jsonArray)
    {
        List<FediPostDTO> postsList = new() { };

        foreach (JObject jsonObject in jsonArray.Cast<JObject>())
        {
            jsonObject.TryGetValue("id", out JToken idJson);
            jsonObject.TryGetValue("created_at", out JToken createdAtJson);
            jsonObject.TryGetValue("url", out JToken urlJson);
            jsonObject.TryGetValue("content", out JToken contentJson);

            string id = idJson.Value<string>();
            DateTime createdAt = createdAtJson.Value<DateTime>();
            string content = contentJson.Value<string>();

            string urlString = urlJson.Value<string>();
            Uri url = new(urlString);

            FediPostDTO post = new()
            {
                Id = id,
                CreatedAt = createdAt,
                Url = url,
                Content = content
            };

            postsList.Add(post);
        }

        return postsList.AsReadOnly();
    }

    // TODO: dateStart
    // TODO: dateEnd
    public async Task<ReadOnlyCollection<FediPostDTO>> GetPostsAsync(int limit)
    {
        string userID = await GetUserIdAsync()
            .ConfigureAwait(false);

        Uri url = new($"https://{_fediAccount.FediInstance}/api/v1/accounts/{userID}/statuses?limit={limit}&exclude_reblogs=true");

        using HttpClient httpClient = new();

        try
        {
            HttpResponseMessage response = await httpClient
                .GetAsync(url)
                .ConfigureAwait(false);

            response.EnsureSuccessStatusCode();

            string jsonContent = await response
                .Content
                .ReadAsStringAsync()
                .ConfigureAwait(false);

            JArray jsonArray = JArray.Parse(jsonContent);

            return JsonArrayToPostsList(jsonArray);
        }
        catch (Exception exception)
        {
            throw new Exception("request did not complete successfully", exception);
        }
    }

}
