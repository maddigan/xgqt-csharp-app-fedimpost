;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((find-file . ((require-final-newline . t)
               (show-trailing-whitespace . t)))
 (csharp-mode . ((indent-tabs-mode . nil)))
 (earthfile-mode . ((indent-tabs-mode . nil)
                    (tab-width . 4)))
 (earthfile-mode . ((indent-tabs-mode . nil)
                    (tab-width . 4)))
 (makefile-mode . ((indent-tabs-mode . t)))
 (markdown-mode . ((indent-tabs-mode . nil)))
 (python-mode . ((indent-tabs-mode . nil)
                 (tab-width . 4))))
