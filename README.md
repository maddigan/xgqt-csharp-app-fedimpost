# Fedimpost

<p align="center">
  <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/xgqt-csharp-app-fedimpost/">
    <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/xgqt-csharp-app-fedimpost/">
  </a>
</p>

Fediverse post import to your (my) favorite blog framework(s).

## About

Current major version is 1 and the full version is 1.0.2.

## Development requirements

* GNU Make
* Python >= 3.10
* DotNET SDK >= 6.0

## Dependencies

### DotNET

* `Microsoft.Extensions.Logging`
* `Microsoft.Extensions.Logging`
* `Microsoft.Extensions.Logging.Console`
* `Newtonsoft.Json`
* `ReverseMarkdown`
* `System.CommandLine`

## Building

To build execute:

```shell
make -C Source/v1 build
```

## Testing

To analyze and test execute:

```shell
make -C Source/v1 analyze
```

## Publishing

To publish a self-contained one-file binary execute:

```shell
make -C Source/v1 publish
```

## Running

To run straight from the repository after building execute:

```shell
./Source/v1/admin/exec_fedimpost_app.py
```
